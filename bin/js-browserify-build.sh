#!/bin/sh
for file in `ls ${1} | grep .js`; do
	browserify ${1}${file} -t babelify --outfile ${2}${file}
	uglifyjs ${2}${file} -o ${2}${file}
done
