const browserSync = require('browser-sync');
const proxy = require('proxy-middleware');
const url = require('url');
const config = require('./conifg.js')
const proxyOptions = url.parse(config.api.proxyUrl);
const exec = require('child_process').exec;
proxyOptions.route = config.api.routePath;

const params = config.api.isApi ? {
  port: 8080,
  server:"./src",
  files:"src/assets/preprocessor/**/*",
  middleware:[proxy(proxyOptions)]
} : {
  port: 8080,
  server:"./src",
  files:"src/assets/preprocessor/**/*"
};
browserSync(params)

browserSync.watch('./src/assets/preprocessor/js/*.js').on('change', ()=>{
	exec('npm run build-browserify', function(err, stdout, stderr){
		if (err) { console.log(err); }
	});
 });